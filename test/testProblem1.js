const {createFile, deleteFiles, createDirectory} = require("../problem1");

try{
    createDirectory()
    .then(() => {
        return createFile();
    })
    .then( () => {
        return deleteFiles();
    })
    .catch( (err) => console.log(`Error Occurred: ${err}`))
}catch(err){
    console.log(`Cannot call file due to ${err}`);
}