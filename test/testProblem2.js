const {readFile, writeFile, appendFile, deleteFile} = require ("../problem2");

const path = "./lipsum.txt";

readFile(path)
.then( (data) => {
    console.log("lipsum.txt file read successfully.");
    let upperCaseData = data.toUpperCase();
    return upperCaseData;
})
.then( (upperCaseData) => {
    let newFileName = "upperCaseFile.txt";
    writeFile(upperCaseData, newFileName);
    console.log("upperCaseFile written successfully.");
    return newFileName;
})
.then( (newFileName) => {
    appendFile(newFileName);
    console.log("upperCaseFile name added successfully to fileNames.txt")
    return newFileName;
})
.then( (newFileName) => {
    return readFile(newFileName);
})
.then( (data) => {
    console.log("upperCaseFile.txt read successfully.")
    let lowerCaseData = data.toLowerCase();
    return lowerCaseData;
})
.then( (data) => {
    let paragraphSplit = data.split("\n\n");
    let sentences = "";

    paragraphSplit.forEach((eachPara) => {
        let sentenceline = eachPara.split(". ");
        sentenceline.forEach( (eachSentence) => {
            eachSentence += ".";
            sentences += (eachSentence + "\n");
        })
    })
    return sentences;
})
.then( (sentences) => {
    let newFileName = "lowerCaseFile.txt";
    writeFile(sentences, newFileName);
    console.log("lowerCaseFile written successfully.");
    return newFileName;
})
.then( (newFileName) => {
    appendFile(newFileName);
    console.log("lowerCaseFile name added successfully to fileNames.txt")
    return newFileName;
})
.then( (newFileName) => {
    return readFile(newFileName);
})
.then((data) => {
    console.log("lowerCaseFile.txt read successfully.");
    dataArray = data.split("\n");
    dataArray.sort();
    let sortedArray = JSON.stringify(dataArray);
    return sortedArray;
})
.then( (sortedArray) => {
    let fileName = "sortedFile.txt";
    writeFile(sortedArray, fileName);
    console.log("sortedFile written successfully");
    return fileName;
})
.then( (fileName) => {
    appendFile(fileName);
    console.log("sortedFile name added successfully to fileNames.txt")
    return fileName;
})
.then( () => {
    return readFile("filenames.txt")
})
.then((data) => {
    console.log("filenames.txt read successfully.")
    let promiseArray = [];
    let nameList = data.split("\n");

    nameList.forEach( (nameElement) => {
        if(nameElement !== ""){
            let promise = deleteFile(nameElement);
            promiseArray.push(promise);
        }
    })
    return Promise.all(promiseArray);
})
.then(() => {
    console.log("All files in filenames.txt deleted successfully");
})
.catch((err) => {
    console.log("Error Occured: ", err);
})