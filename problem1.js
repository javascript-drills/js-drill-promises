// Using callbacks and the fs module's asynchronous functions, 
// do the following:
// 1. Create a directory of random JSON files
// 2. Delete those files simultaneously 


const fs = require("fs").promises;


function createDirectory(){
    return fs.mkdir("folder", {recursive: true})
}

function createFile(){
    let creationPromises = [];
    for( let index=0; index<5; index++){
        let promise = fs.writeFile(`folder/File-${index+1}.json`, "Hello");
        creationPromises.push(promise);
        console.log(`Created file-${index+1}`);
    }
    return Promise.all(creationPromises);
}

function deleteFiles(){
    let deletionPromises = [];
    for( let index=0; index<5; index++ ){
        let delPromise = fs.unlink(`folder/File-${index+1}.json`);
        deletionPromises.push(delPromise);
        console.log(`Deleted file-${index+1}`);
    }
    return Promise.all(deletionPromises);
}

module.exports = {createFile, deleteFiles, createDirectory};